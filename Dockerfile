FROM openjdk:17-alpine
EXPOSE 8080
COPY target/drive-and-deliver.jar /app/drive-and-deliver.jar
ENTRYPOINT ["java", "-jar", "demo.jar"]