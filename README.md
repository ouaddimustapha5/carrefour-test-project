Introduction

Ce projet est une application de livraison construite en utilisant Spring Boot. Il permet aux clients de choisir leur mode de livraison et de planifier des créneaux horaires pour leurs livraisons. Le projet utilise des technologies modernes telles que Spring Native pour la création d'images natives, Spring Security pour la sécurisation de l'API, et Kafka pour la gestion des événements de streaming de données.
Choix d'implémentation
Langages et Frameworks

    Java 17: Utilisé pour profiter des dernières fonctionnalités du langage et des améliorations de performance.
    Spring Boot 3.x.x: Framework principal pour le développement de l'application.
    Spring Data JPA: Pour l'accès et la gestion des données.
    H2 Database: Base de données en mémoire pour les tests et le développement initial.
    MySQL: Base de données relationnelle utilisée pour le développement.
    PostgreSQL: Base de données relationnelle pour la production.
    Spring Cache avec Redis: Pour améliorer la performance grâce à la mise en cache.
    Kafka: Pour produire et consommer des événements de streaming de données.
    Docker: Pour containeriser l'application.
    Kubernetes: Pour déployer l'application dans un pod.
    Spring Native: Pour créer des images natives de l'application.

Sécurité

    Spring Security: Utilisé pour sécuriser l'API REST.

Documentation API

    Swagger: Utilisé pour documenter l'API REST.

Étapes pour lancer le projet
Prérequis

    Java 21
    Git
    Maven
    Docker
    Kafka
    Redis

Configuration de l'environnement

    Installer Java 21

    Installer Git

    Utiliser Spring Initializr pour créer un projet Spring Boot 3.x.x avec les dépendances nécessaires:
        Spring Web
        Spring Data JPA
        H2 Database

    Configurer MySQL pour le développement:

    yaml

spring:
datasource:
url: jdbc:mysql://localhost:3306/driveanddeliver
username: yourusername
password: yourpassword
driver-class-name: com.mysql.cj.jdbc.Driver
jpa:
hibernate:
ddl-auto: update
show-sql: true

Configurer PostgreSQL pour la production:

yaml

    spring:
      datasource:
        url: jdbc:postgresql://localhost:5432/driveanddeliver
        username: yourusername
        password: yourpassword
        driver-class-name: org.postgresql.Driver
      jpa:
        hibernate:
          ddl-auto: update
        show-sql: true

Création du projet de base

    Générer le projet avec Spring Initializr:
        Télécharger le projet généré depuis Spring Initializr.
        Extraire le projet généré.

    Initialiser un dépôt Git dans le répertoire du projet:

    sh

    git init
    git add .
    git commit -m "Initial commit"

Développement du MVP
User Story 1 : Choix du mode de livraison

    Créer un modèle de données pour les méthodes de livraison (enum).
    Créer une entité pour les clients.
    Créer une entité pour les choix de livraison.
    Créer des repositories pour les entités.
    Créer un service pour gérer la logique métier des choix de livraison.
    Créer un contrôleur pour exposer les endpoints de l'API REST.

User Story 2 : Choix du jour et du créneau horaire

    Ajouter des champs pour la date de livraison et le créneau horaire dans l'entité des choix de livraison.
    Mettre à jour le service pour inclure la logique de récupération des créneaux horaires disponibles.
    Mettre à jour le contrôleur pour inclure un endpoint permettant de récupérer les créneaux horaires disponibles.

Améliorations et fonctionnalités bonus
REST API

    Documenter l'API avec Swagger.
    Sécuriser l'API avec Spring Security.
    Implémenter HATEOAS pour enrichir les réponses de l'API.
    Utiliser Spring WebFlux pour une solution non bloquante.

Persistence

    Utiliser une base de données relationnelle (MySQL pour le développement, PostgreSQL pour la production).
    Utiliser Spring Cache avec Redis pour la mise en cache.

Stream

    Utiliser Kafka pour produire et consommer des événements de streaming de données.

CI/CD

    Configurer GitLab CI/CD pour l'intégration continue et le déploiement continu.

Packaging

    Utiliser Docker pour créer une image de l'application.
    Utiliser Kubernetes pour déployer l'application dans un pod.
    Utiliser Spring Native pour créer une image native de l'application.
