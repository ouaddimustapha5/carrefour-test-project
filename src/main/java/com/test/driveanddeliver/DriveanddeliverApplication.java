package com.test.driveanddeliver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
//@EnableCaching
public class DriveanddeliverApplication {

	public static void main(String[] args) {
		SpringApplication.run(DriveanddeliverApplication.class, args);
	}

}
