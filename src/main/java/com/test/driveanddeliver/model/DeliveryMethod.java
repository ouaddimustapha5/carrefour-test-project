package com.test.driveanddeliver.model;

public enum DeliveryMethod {
    DRIVE,
    DELIVERY,
    DELIVERY_TODAY,
    DELIVERY_ASAP
}
