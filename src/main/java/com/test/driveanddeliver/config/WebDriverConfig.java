package com.test.driveanddeliver.config;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class WebDriverConfig {

    @Bean
    public WebDriver webDriver() {
        System.setProperty("webdriver.chrome.driver", "path/to/chromedriver"); // Remplacez par le chemin de votre ChromeDriver
        return new ChromeDriver();
    }
}
