package com.test.driveanddeliver.controller;

import com.test.driveanddeliver.entity.DeliveryChoice;
import com.test.driveanddeliver.model.DeliveryMethod;
import com.test.driveanddeliver.service.DeliveryChoiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping("/api/delivery-choices")
public class DeliveryChoiceController {

    @Autowired
    private DeliveryChoiceService deliveryChoiceService;

    @GetMapping
    public List<EntityModel<DeliveryChoice>> getAllDeliveryChoices() {
        List<DeliveryChoice> deliveryChoices = deliveryChoiceService.getAllDeliveryChoices();
        return deliveryChoices.stream()
                .map(deliveryChoice -> EntityModel.of(deliveryChoice,
                        WebMvcLinkBuilder.linkTo(methodOn(DeliveryChoiceController.class).getDeliveryChoiceById(deliveryChoice.getId())).withSelfRel()))
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public EntityModel<DeliveryChoice> getDeliveryChoiceById(@PathVariable Long id) {
        DeliveryChoice deliveryChoice = deliveryChoiceService.getDeliveryChoiceById(id);
        return EntityModel.of(deliveryChoice,
                WebMvcLinkBuilder.linkTo(methodOn(DeliveryChoiceController.class).getDeliveryChoiceById(id)).withSelfRel());
    }

    @PostMapping
    public DeliveryChoice createDeliveryChoice(@RequestBody DeliveryChoice deliveryChoice) {
        return deliveryChoiceService.saveDeliveryChoice(deliveryChoice);
    }

    @GetMapping("/available-time-slots")
    public List<String> getAvailableTimeSlots(@RequestParam DeliveryMethod deliveryMethod, @RequestParam LocalDateTime deliveryDate) {
        return deliveryChoiceService.getAvailableTimeSlots(deliveryMethod, deliveryDate);
    }
}
