package com.test.driveanddeliver.repository;


import com.test.driveanddeliver.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.redis.core.RedisHash;

@RedisHash
public interface CustomerRepository extends JpaRepository<Customer, Long> {
}
