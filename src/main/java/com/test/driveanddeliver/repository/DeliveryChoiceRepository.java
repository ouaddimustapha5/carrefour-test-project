package com.test.driveanddeliver.repository;

import com.test.driveanddeliver.entity.DeliveryChoice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.redis.core.RedisHash;

@RedisHash
public interface DeliveryChoiceRepository extends JpaRepository<DeliveryChoice, Long> {
}
