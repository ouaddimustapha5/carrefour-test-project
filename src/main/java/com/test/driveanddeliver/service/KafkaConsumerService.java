package com.test.driveanddeliver.service;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class KafkaConsumerService {

    @KafkaListener(topics = "delivery_topic", groupId = "delivery-group")
    public void consumeMessage(String message) {
        System.out.println("Received message: " + message);
    }
}
