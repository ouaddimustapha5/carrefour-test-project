package com.test.driveanddeliver.service;

import com.test.driveanddeliver.entity.DeliveryChoice;
import com.test.driveanddeliver.model.DeliveryMethod;
import com.test.driveanddeliver.repository.DeliveryChoiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class DeliveryChoiceService {

    @Autowired
    private DeliveryChoiceRepository deliveryChoiceRepository;

    @Cacheable(value = "deliveryChoices")
    public List<DeliveryChoice> getAllDeliveryChoices() {
        return deliveryChoiceRepository.findAll();
    }

    @CachePut(value = "deliveryChoices", key = "#result.id")
    public DeliveryChoice saveDeliveryChoice(DeliveryChoice deliveryChoice) {
        return deliveryChoiceRepository.save(deliveryChoice);
    }

    public List<String> getAvailableTimeSlots(DeliveryMethod deliveryMethod, LocalDateTime deliveryDate) {
        // Logic to get available time slots, for now just returning dummy data
        List<String> timeSlots = new ArrayList<>();
        timeSlots.add("09:00 - 10:00");
        timeSlots.add("10:00 - 11:00");
        timeSlots.add("11:00 - 12:00");
        return timeSlots;
    }

    @Cacheable(value = "deliveryChoices", key = "#id")
    public DeliveryChoice getDeliveryChoiceById(Long id) {
        Optional<DeliveryChoice> deliveryChoice = deliveryChoiceRepository.findById(id);
        return deliveryChoice.orElse(null);
    }
}
