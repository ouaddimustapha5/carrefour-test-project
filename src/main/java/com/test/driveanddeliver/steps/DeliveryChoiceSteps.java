package com.test.driveanddeliver.steps;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;

public class DeliveryChoiceSteps {

    @Autowired
    private WebDriver webDriver;

    @Given("the user is on the delivery page")
    public void theUserIsOnTheDeliveryPage() {
        webDriver.get("http://localhost:8080/delivery"); // Remplacez par l'URL correcte de votre page de livraison
    }

    @When("the user selects {string} as the delivery method")
    public void theUserSelectsAsTheDeliveryMethod(String method) {
        webDriver.findElement(By.id("deliveryMethod")).sendKeys(method);
    }

    @Then("the delivery method should be {string}")
    public void theDeliveryMethodShouldBe(String expectedMethod) {
        String selectedMethod = webDriver.findElement(By.id("deliveryMethod")).getAttribute("value");
        assertEquals(expectedMethod, selectedMethod);
    }
}
