Feature: Delivery Choice
  Scenario: User chooses a delivery method
    Given the user is on the delivery page
    When the user selects "DELIVERY" as the delivery method
    Then the delivery method should be "DELIVERY"
