package com.test.driveanddeliver.service;

import com.test.driveanddeliver.entity.DeliveryChoice;
import com.test.driveanddeliver.model.DeliveryMethod;
import com.test.driveanddeliver.repository.DeliveryChoiceRepository;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class DeliveryChoiceServiceTest {
/*
    @Mock
    private DeliveryChoiceRepository deliveryChoiceRepository;

    @InjectMocks
    private DeliveryChoiceService deliveryChoiceService;

    public DeliveryChoiceServiceTest() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testGetAllDeliveryChoices() {
        List<DeliveryChoice> deliveryChoices = new ArrayList<>();
        deliveryChoices.add(new DeliveryChoice());

        when(deliveryChoiceRepository.findAll()).thenReturn(deliveryChoices);

        List<DeliveryChoice> result = deliveryChoiceService.getAllDeliveryChoices();

        assertEquals(1, result.size());
        verify(deliveryChoiceRepository, times(1)).findAll();
    }

    @Test
    public void testSaveDeliveryChoice() {
        DeliveryChoice deliveryChoice = new DeliveryChoice();
        when(deliveryChoiceRepository.save(deliveryChoice)).thenReturn(deliveryChoice);

        DeliveryChoice result = deliveryChoiceService.saveDeliveryChoice(deliveryChoice);

        assertEquals(deliveryChoice, result);
        verify(deliveryChoiceRepository, times(1)).save(deliveryChoice);
    }

    @Test
    public void testGetAvailableTimeSlots() {
        List<String> timeSlots = deliveryChoiceService.getAvailableTimeSlots(DeliveryMethod.DELIVERY, LocalDateTime.now());

        assertNotNull(timeSlots);
        assertTrue(timeSlots.size() > 0);
    }

    @Test
    public void testGetDeliveryChoiceById() {
        DeliveryChoice deliveryChoice = new DeliveryChoice();
        when(deliveryChoiceRepository.findById(1L)).thenReturn(Optional.of(deliveryChoice));

        DeliveryChoice result = deliveryChoiceService.getDeliveryChoiceById(1L);

        assertEquals(deliveryChoice, result);
        verify(deliveryChoiceRepository, times(1)).findById(1L);
    }*/
}