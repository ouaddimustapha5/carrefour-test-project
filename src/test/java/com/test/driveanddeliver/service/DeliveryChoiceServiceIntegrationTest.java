package com.test.driveanddeliver.service;

import com.test.driveanddeliver.entity.DeliveryChoice;
import com.test.driveanddeliver.model.DeliveryMethod;
import com.test.driveanddeliver.repository.DeliveryChoiceRepository;
import jakarta.transaction.Transactional;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
/*
@SpringBootTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Transactional*/
public class DeliveryChoiceServiceIntegrationTest {
/*
    @Autowired
    private DeliveryChoiceService deliveryChoiceService;

    @Autowired
    private DeliveryChoiceRepository deliveryChoiceRepository;

    @Test
    public void testGetAllDeliveryChoices() {
        List<DeliveryChoice> deliveryChoices = deliveryChoiceService.getAllDeliveryChoices();
        assertNotNull(deliveryChoices);
    }

    @Test
    public void testSaveDeliveryChoice() {
        DeliveryChoice deliveryChoice = new DeliveryChoice();
        deliveryChoice.setDeliveryMethod(DeliveryMethod.DELIVERY);
        deliveryChoice.setDeliveryDate(LocalDateTime.now());
        DeliveryChoice savedDeliveryChoice = deliveryChoiceService.saveDeliveryChoice(deliveryChoice);

        assertNotNull(savedDeliveryChoice.getId());
    }

    @Test
    public void testGetAvailableTimeSlots() {
        List<String> timeSlots = deliveryChoiceService.getAvailableTimeSlots(DeliveryMethod.DELIVERY, LocalDateTime.now());

        assertNotNull(timeSlots);
        assertTrue(timeSlots.size() > 0);
    }

    @Test
    public void testGetDeliveryChoiceById() {
        DeliveryChoice deliveryChoice = new DeliveryChoice();
        deliveryChoice.setDeliveryMethod(DeliveryMethod.DELIVERY);
        deliveryChoice.setDeliveryDate(LocalDateTime.now());
        deliveryChoice = deliveryChoiceRepository.save(deliveryChoice);

        DeliveryChoice foundDeliveryChoice = deliveryChoiceService.getDeliveryChoiceById(deliveryChoice.getId());

        assertNotNull(foundDeliveryChoice);
        assertEquals(deliveryChoice.getId(), foundDeliveryChoice.getId());
    }*/
}